# swagger_client.AccountApi

All URIs are relative to *https://dex.binance.org/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_account**](AccountApi.md#get_account) | **GET** /account/{address} | Add a new pet to the store
[**get_account_sequence**](AccountApi.md#get_account_sequence) | **GET** /account/{address}/sequence | Get an account sequence
[**get_closed_orders**](AccountApi.md#get_closed_orders) | **GET** /orders/closed | Get closed orders
[**get_orders**](AccountApi.md#get_orders) | **GET** /orders/{orderId} | Get orders


# **get_account**
> AccountMeta get_account(address)

Add a new pet to the store



### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AccountApi()
address = 'address_example' # str | The account address to query

try:
    # Add a new pet to the store
    api_response = api_instance.get_account(address)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountApi->get_account: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| The account address to query | 

### Return type

[**AccountMeta**](AccountMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_sequence**
> get_account_sequence(address)

Get an account sequence



### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AccountApi()
address = 'address_example' # str | The account address to query

try:
    # Get an account sequence
    api_instance.get_account_sequence(address)
except ApiException as e:
    print("Exception when calling AccountApi->get_account_sequence: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| The account address to query | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_closed_orders**
> ClosedOrderInfo get_closed_orders(address)

Get closed orders



### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AccountApi()
address = 'address_example' # str | The account address to query

try:
    # Get closed orders
    api_response = api_instance.get_closed_orders(address)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountApi->get_closed_orders: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| The account address to query | 

### Return type

[**ClosedOrderInfo**](ClosedOrderInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_orders**
> OrderInfo get_orders(order_id)

Get orders



### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AccountApi()
order_id = 'order_id_example' # str | The Order Id

try:
    # Get orders
    api_response = api_instance.get_orders(order_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountApi->get_orders: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **str**| The Order Id | 

### Return type

[**OrderInfo**](OrderInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

