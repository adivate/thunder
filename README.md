## General:
The creation of the bot is inspired both by ThorChain community (https://medium.com/@thorchain/rune-arb-bot-competition-478a72c86210) and my own school project

A detailed paper on the preparation and planning of the project can be found in /docs, a complete project report will be released later as well

Shout out to entire ThorChain community, Kai, and Sonya for helping

## Credits:
Used Python SDK(python-sdk) to broadcast message:
https://github.com/binance-chain/python-sdk/

Used swagger-editor to generate the rest of client-side api code(python-client):
https://github.com/swagger-api/swagger-editor

Learned Arbitrage here, will implement more sophisticated algo later on:
https://web.stanford.edu/~guillean/papers/uniswap_analysis.pdf

## WHAT WORKS:
- [ Import wallet keys locally ] 
- [ Compare markets, log trade opportunities ]
- [ Open and close orders to take trade opportunities ]
- [ Log a running PNL ]
- [ Testing ]
- [ Simple FrontEnd ]
## LEFT TODO:
- [ Integrate Backend and FrontEnd ]
- [ Add auto-optimization for API request rate ]
- [ Add backup plan in case trade didn't go through ]
- [ Add support for more currencies: import token name, lotsize, ticksize ]

## Requirement
python=3.5.6

## Install (Using Python3.5.6)
    git clone https://gitlab.com/zheye/thunder.git
    
    cd thunder
    
### Optional

    python3.5 -m venv env
    
    source env/bin/activate
### Install future-fstrings due to compatibility issue in python-sdk
    pip install future-fstrings
### Install swagger generated client side code
    pip install python-client/
### Install unofficial sdk for broadcasting msgs
    pip install python-sdk/

## Configure and Run
    cd src
    python driver.py -k [your private key]
    ### FrontEnd
    cd Thunder
    python manage.py runserver
## Test
    cd test
    pytest *.py
![Test](/readme_img/testcases.png)
## Functionality
Upon starting, the bot starts to look for market condition (all parameters will be configurable later on)

![Beta](/readme_img/beta_release.png)

Bot reporting issue on liquidity
![Liquidity](/readme_img/liquidity_report.png)

The trade logs and running pnl is currently saved to a local file, will integrate the logs into frontend soon
## FrontEnd Demo
Learning FrontEnd for the first time, the idea is press the rad icon to start running, and there's a gif that will be displayed everytime a trading condition is satisfied

![FrontEnd](/readme_img/frontend.png)

