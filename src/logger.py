import time
import swagger_client
from swagger_client.rest import ApiException


class Logger:
    def __init__(self, tokens, address):
        self.pairs = {
            "BNB": "BNB",
            "RUNE": "RUNE-B1A",
            "BUSD": "BUSD-BD1",
        }
        self.balances = {
            "BNB": 0,
            "RUNE": 0,
            "BUSD": 0,
        }
        self.address = address
        # create an instance of the API class
        self.api_instance = swagger_client.AccountApi()
        for token in tokens:
            try:
                # Get an account.
                api_response = self.api_instance.get_account(self.address)
                tracking = list(filter(lambda coin: coin['symbol'] == self.pairs[token], api_response.balances))
                self.balances[token] = float(tracking[0]['free'])
                # export to sql
            except ApiException as e:
                print("Exception when calling AccountApi->getAccount: %s\n" % e)
            print("Started logging with token: %s, starting balance is %s" % (token, self.balances[token]))

    ''' Track pnl of the specified coin '''

    def log_pnl(self, token):
        try:
            # Get an account.
            api_response = self.api_instance.get_account(self.address)
            tracking = list(filter(lambda coin: coin['symbol'] == self.pairs[token], api_response.balances))
            pnl = (float(tracking[0]['free']) - float(self.balances[token])) / float(self.balances[token])
            self.balances[token] = float(tracking[0]['free'])
            print("your pnl since last arbitrage: %f percent" % (pnl*100))
            print("your new balance is: %f" % self.balances[token])
            return pnl
        except ApiException as e:
            print("Exception when calling AccountApi->getAccount: %s\n" % e)

    def log_new_trade(self, order_id):
        try:
            file = open("debug.txt", "a")
            file.write(order_id)
            file.write('\n')
            file.close()
            api_response = self.api_instance.get_orders(order_id)
            print("Logging new trade %s" % order_id)
            return api_response.status == "FullyFill"
        except ApiException as e:
            print("Exception when calling AccountApi->getOrder: %s\n" % e)
            return 0
