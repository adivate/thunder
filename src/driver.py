import sys

sys.path.append("..")
from src import Hunter
import argparse

# Instantiate the parser
parser = argparse.ArgumentParser(description='Rune Arbitrager')
parser.add_argument("-k", "--key", type=str, required=True)
args = parser.parse_args()
x = Hunter(args.key)
x.hunt()