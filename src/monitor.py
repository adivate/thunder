import time
import swagger_client
from decimal import Decimal
from swagger_client.rest import ApiException
from binance_chain.http import HttpApiClient
from binance_chain.messages import NewOrderMsg
from binance_chain.wallet import Wallet
from binance_chain.environment import BinanceEnvironment


class LiquidityError(Exception):
    pass


class Monitor:
    def __init__(self, wallet, base_size, fee, omega, ):
        self.pairs = {
            "bnb.busd": "BNB_BUSD-BD1",
            "rune.bnb": "RUNE-B1A_BNB",
            "rune.busd": "RUNE-B1A_BUSD-BD1",
        }
        self.wallet = wallet
        self.base_size = base_size
        self.fee = fee
        self.omega = omega
        self.alpha_msg = None
        self.beta_msg = None
        self.theta_msg = None
        self.alpha_lot_size = 3
        self.alpha_tick_size = 4
        self.beta_lot_size = 1
        self.beta_tick_size = 7
        self.api_instance = swagger_client.MarketApi()
        print("Monitor initialized")

    # Refer Lot size to marketpairs.json
    ''' Scan and construct message for broadcast '''

    def scan(self, token):
        try:
            # Strategy for BUSD gain, buy bnb, buy rune, sell rune
            if token == "BUSD":
                print("Monitoring ---")
                # Get the order book.
                # api_response.asks | api_response.bids
                bnb_busd = self.api_instance.get_depth(self.pairs["bnb.busd"], limit=5)
                rune_bnb = self.api_instance.get_depth(self.pairs["rune.bnb"], limit=5)
                rune_busd = self.api_instance.get_depth(self.pairs["rune.busd"], limit=5)
                # Delta BNB (BNB obtained by selling BUSD)
                alpha = self.base_size / float(bnb_busd.asks[0][0])
                alpha = round(alpha, 4)
                # Delta RUNE (RUNE obtained by selling BNB)
                beta = alpha / float(rune_bnb.asks[0][0])
                beta = round(beta, 1)
                # Delta BUSD (BUSD obtained by selling RUNE)
                theta = float(rune_busd.bids[0][0]) * beta
                profit = theta - self.base_size * (1 + self.fee * 3)
                bnb_liquidity = float(bnb_busd.asks[0][1]) >= alpha * self.omega
                rune_liquidity = float(rune_bnb.asks[0][1]) >= beta * self.omega
                busd_liquidity = float(rune_busd.bids[0][1]) >= beta * self.omega
                if not bnb_liquidity:
                    print(bnb_busd)
                    raise LiquidityError("BNB/BUSD")
                if not rune_liquidity:
                    print(rune_bnb)
                    raise LiquidityError("RUNE/BNB")
                if not busd_liquidity:
                    print(rune_busd)
                    raise LiquidityError("RUNE/BUSD")
                if bnb_liquidity and rune_liquidity and busd_liquidity:
                    if profit > 0:
                        # Construct Message for Alpha Order
                        self.alpha_msg = NewOrderMsg(
                            wallet=self.wallet,
                            symbol=self.pairs["bnb.busd"],
                            time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                            order_type=2,  # only accept 2 for now, meaning limit order
                            side=1,  # 1 for buy and 2 for sell
                            price=float(bnb_busd.asks[0][0]),
                            quantity=alpha,
                        )
                        # Construct Message for Beta Order
                        self.beta_msg = NewOrderMsg(
                            wallet=self.wallet,
                            symbol=self.pairs["rune.bnb"],
                            time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                            order_type=2,  # only accept 2 for now, meaning limit order
                            side=1,  # 1 for buy and 2 for sell
                            price=float(rune_bnb.asks[0][0]),
                            quantity=beta,
                        )
                        # Construct Message for Theta Order
                        self.theta_msg = NewOrderMsg(
                            wallet=self.wallet,
                            symbol=self.pairs["rune.busd"],
                            time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                            order_type=2,  # only accept 2 for now, meaning limit order
                            side=2,  # 1 for buy and 2 for sell
                            price=float(rune_busd.bids[0][0]),
                            quantity=beta,
                        )
                    return profit
        except ApiException as e:
            print("Exception when calling MarketApi->getDepth: %s\n" % e)
            return 0

    ''' Test Function: takes json input '''

    def test_scan(self, bnb_busd, rune_bnb, rune_busd):
        # Delta BNB
        alpha = self.base_size / float(bnb_busd['asks'][0][0])
        alpha = round(alpha, self.alpha_lot_size)
        # Delta RUNE
        beta = alpha / float(rune_bnb['asks'][0][0])
        beta = round(beta, self.beta_lot_size)
        # Delta BUSD
        theta = float(rune_busd['bids'][0][0]) * beta
        profit = theta - self.base_size * (1 + self.fee * 3)
        bnb_liquidity = float(bnb_busd['asks'][0][1]) >= alpha * self.omega
        rune_liquidity = float(rune_bnb['asks'][0][1]) >= beta * self.omega
        busd_liquidity = float(rune_busd['bids'][0][1]) >= beta * self.omega
        if not bnb_liquidity:
            raise LiquidityError("BNB/BUSD")
        if not rune_liquidity:
            raise LiquidityError("RUNE/BNB")
        if not busd_liquidity:
            raise LiquidityError("RUNE/BUSD")
        if bnb_liquidity and rune_liquidity and busd_liquidity:
            # if profit > 0:
            # Construct Message for Alpha Order
            self.alpha_msg = NewOrderMsg(
                wallet=self.wallet,
                symbol=self.pairs["bnb.busd"],
                time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                order_type=2,  # only accept 2 for now, meaning limit order
                side=1,  # 1 for buy and 2 for sell
                price=float(bnb_busd['asks'][0][0]),
                quantity=alpha,
            )
            # Construct Message for Beta Order
            self.beta_msg = NewOrderMsg(
                wallet=self.wallet,
                symbol=self.pairs["rune.bnb"],
                time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                order_type=2,  # only accept 2 for now, meaning limit order
                side=1,  # 1 for buy and 2 for sell
                price=float(rune_bnb['asks'][0][0]),
                quantity=beta,
            )
            # Construct Message for Theta Order
            self.theta_msg = NewOrderMsg(
                wallet=self.wallet,
                symbol=self.pairs["rune.busd"],
                time_in_force=3,  # 1 for Good Till Expire(GTE) order and 3 for Immediate Or Cancel (IOC)
                order_type=2,  # only accept 2 for now, meaning limit order
                side=2,  # 1 for buy and 2 for sell
                price=float(rune_busd['bids'][0][0]),
                quantity=beta,
            )
            return profit
