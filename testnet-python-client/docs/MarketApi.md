# swagger_client.MarketApi

All URIs are relative to *https://testnet-dex.binance.org/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_depth**](MarketApi.md#get_depth) | **GET** /depth | Get the order book.
[**get_pairs**](MarketApi.md#get_pairs) | **GET** /markets | Get market pairs.


# **get_depth**
> MarketDepth get_depth(symbol, limit=limit)

Get the order book.

Gets the order book depth data for a given pair symbol.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: petstore_auth
configuration = swagger_client.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.MarketApi(swagger_client.ApiClient(configuration))
symbol = 'symbol_example' # str | Market pair symbol,
limit = 56 # int | The limit of results. Allowed limits: [5, 10, 20, 50, 100, 500, 1000] (optional)

try:
    # Get the order book.
    api_response = api_instance.get_depth(symbol, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MarketApi->get_depth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **symbol** | **str**| Market pair symbol, | 
 **limit** | **int**| The limit of results. Allowed limits: [5, 10, 20, 50, 100, 500, 1000] | [optional] 

### Return type

[**MarketDepth**](MarketDepth.md)

### Authorization

[petstore_auth](../README.md#petstore_auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pairs**
> get_pairs()

Get market pairs.

Gets the list of market pairs that have been listed.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.MarketApi()

try:
    # Get market pairs.
    api_instance.get_pairs()
except ApiException as e:
    print("Exception when calling MarketApi->get_pairs: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

